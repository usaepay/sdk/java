package usaepay.products;

import usaepay.API;

import java.util.Map;
import java.util.HashMap;


public class Categories{

	public static String get(Map data){
		String path="";

		HashMap params = new HashMap<String, String>();
		if(data.containsKey("limit")) params.put("limit",data.get("limit"));
		if(data.containsKey("offset")) params.put("offset",data.get("offset"));

		path = "/products/categories";

		if(data.containsKey("category_key")){
			path = "/products/categories/" + data.get("category_key");
			data.remove("category_key");
		}

		try{
			return API.runCall("get",path,data,params);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}

	public static String post(Map data){
		String path="";


		path = "/products/categories";

		try{
			return API.runCall("post",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}

	public static String delete(Map data){
		String path="";


		if(data.containsKey("category_key")){
			path = "/products/categories/" + data.get("category_key");
			data.remove("category_key");
		}

		try{
			return API.runCall("delete",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}

	public static String put(Map data){
		String path="";


		if(data.containsKey("category_key")){
			path = "/products/categories/" + data.get("category_key");
			data.remove("category_key");
		}

		try{
			return API.runCall("put",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
