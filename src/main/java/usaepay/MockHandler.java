package usaepay;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Base64;
import java.util.Map;
import java.util.HashMap;
import com.google.gson.*;

import javax.net.ssl.*;


public class MockHandler {
    static String response = "No response set";

   static String request="";

   static String path="";

   static String method="";

    public static String mockCall(String input_method, String input_path, String input_data){

        method = input_method;
        path = input_path;
        request = input_data;
        return response;

    }

    public static void reset(){
        method = "";
        path = "";
        request = "";
        response = "No response set";
    }
}
