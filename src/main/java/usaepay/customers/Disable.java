package usaepay.customers;

import usaepay.API;

import java.util.Map;
import java.util.HashMap;


public class Disable{

	public static String post(Map data){
		String path="";


		path = "/customers/disable";

		try{
			return API.runCall("post",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
