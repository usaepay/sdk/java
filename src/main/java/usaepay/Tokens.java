package usaepay;

import usaepay.API;

import java.util.Map;
import java.util.HashMap;


public class Tokens{

	public static String post(Map data){
		String path="";


		path = "/tokens";

		try{
			return API.runCall("post",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}

	public static String get(Map data){
		String path="";


		if(data.containsKey("cardref")){
			path = "/tokens/" + data.get("cardref");
			data.remove("cardref");
		}

		try{
			return API.runCall("get",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
