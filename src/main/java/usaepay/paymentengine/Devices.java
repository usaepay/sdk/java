package usaepay.paymentengine;

import usaepay.API;

import java.util.Map;
import java.util.HashMap;


public class Devices{

	public static String get(Map data){
		String path="";

		HashMap params = new HashMap<String, String>();
		if(data.containsKey("limit")) params.put("limit",data.get("limit"));
		if(data.containsKey("offset")) params.put("offset",data.get("offset"));

		path = "/paymentengine/devices";

		if(data.containsKey("devicekey")){
			path = "/paymentengine/devices/" + data.get("devicekey");
			data.remove("devicekey");
		}

		try{
			return API.runCall("get",path,data,params);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}

	public static String post(Map data){
		String path="";


		path = "/paymentengine/devices";

		try{
			return API.runCall("post",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}

	public static String delete(Map data){
		String path="";


		if(data.containsKey("devicekey")){
			path = "/paymentengine/devices/" + data.get("devicekey");
			data.remove("devicekey");
		}

		try{
			return API.runCall("delete",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}

	public static String put(Map data){
		String path="";


		if(data.containsKey("devicekey")){
			path = "/paymentengine/devices/" + data.get("devicekey");
			data.remove("devicekey");
		}

		try{
			return API.runCall("put",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
