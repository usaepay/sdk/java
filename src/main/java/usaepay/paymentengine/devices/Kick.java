package usaepay.paymentengine.devices;

import usaepay.API;

import java.util.Map;
import java.util.HashMap;


public class Kick{

	public static String post(Map data){
		String path="";

	
if(!data.containsKey("devicekey")){
			HashMap res = new HashMap<String, String>();
			res.put("ErrorMessage","Missing devicekey");
			return API.json_encode(res);
		}


		path = "/paymentengine/devices/" + data.get("devicekey") + "/kick";

		try{
			return API.runCall("post",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
