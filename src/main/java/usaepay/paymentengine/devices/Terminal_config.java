package usaepay.paymentengine.devices;

import usaepay.API;

import java.util.Map;
import java.util.HashMap;


public class Terminal_config{

	public static String get(Map data){
		String path="";

	
if(!data.containsKey("devicekey")){
			HashMap res = new HashMap<String, String>();
			res.put("ErrorMessage","Missing devicekey");
			return API.json_encode(res);
		}


		path = "/paymentengine/devices/" + data.get("devicekey") + "/terminal-config";

		try{
			return API.runCall("get",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}

	public static String put(Map data){
		String path="";

	
if(!data.containsKey("devicekey")){
			HashMap res = new HashMap<String, String>();
			res.put("ErrorMessage","Missing devicekey");
			return API.json_encode(res);
		}


		path = "/paymentengine/devices/" + data.get("devicekey") + "/terminal-config";

		try{
			return API.runCall("put",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
