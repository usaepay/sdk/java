package usaepay.paymentengine;

import usaepay.API;

import java.util.Map;
import java.util.HashMap;


public class Payrequests{

	public static String post(Map data){
		String path="";


		path = "/paymentengine/payrequests";

		try{
			return API.runCall("post",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}

	public static String get(Map data){
		String path="";


		if(data.containsKey("requestkey")){
			path = "/paymentengine/payrequests/" + data.get("requestkey");
			data.remove("requestkey");
		}

		try{
			return API.runCall("get",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}

	public static String delete(Map data){
		String path="";


		if(data.containsKey("requestkey")){
			path = "/paymentengine/payrequests/" + data.get("requestkey");
			data.remove("requestkey");
		}

		try{
			return API.runCall("delete",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
