package usaepay;

import usaepay.API;

import java.util.Map;
import java.util.HashMap;


public class Batches{

	public static String get(Map data){
		String path="";

		HashMap params = new HashMap<String, String>();
		if(data.containsKey("limit")) params.put("limit",data.get("limit"));
		if(data.containsKey("offset")) params.put("offset",data.get("offset"));
		if(data.containsKey("openedlt")) params.put("openedlt",data.get("openedlt"));
		if(data.containsKey("openedgt")) params.put("openedgt",data.get("openedgt"));
		if(data.containsKey("closedlt")) params.put("closedlt",data.get("closedlt"));
		if(data.containsKey("closedgt")) params.put("closedgt",data.get("closedgt"));
		if(data.containsKey("openedle")) params.put("openedle",data.get("openedle"));
		if(data.containsKey("openedge")) params.put("openedge",data.get("openedge"));
		if(data.containsKey("closedle")) params.put("closedle",data.get("closedle"));
		if(data.containsKey("closedge")) params.put("closedge",data.get("closedge"));

		path = "/batches";

		if(data.containsKey("batch_key")){
			path = "/batches/" + data.get("batch_key");
			data.remove("batch_key");
		}

		try{
			return API.runCall("get",path,data,params);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
