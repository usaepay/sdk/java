package usaepay;

import usaepay.API;

import java.util.Map;
import java.util.HashMap;


public class Transactions{

	public static String get(Map data){
		String path="";

		HashMap params = new HashMap<String, String>();
		if(data.containsKey("limit")) params.put("limit",data.get("limit"));
		if(data.containsKey("offset")) params.put("offset",data.get("offset"));
		if(data.containsKey("fuzzy")) params.put("fuzzy",data.get("fuzzy"));
		if(data.containsKey("filters")) params.put("filters",data.get("filters"));

		path = "/transactions";

		if(data.containsKey("trankey")){
			path = "/transactions/" + data.get("trankey");
			data.remove("trankey");
		}

		try{
			return API.runCall("get",path,data,params);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}

	public static String post(Map data){
		String path="";


		path = "/transactions";

		try{
			return API.runCall("post",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
