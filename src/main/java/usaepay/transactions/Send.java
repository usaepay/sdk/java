package usaepay.transactions;

import usaepay.API;

import java.util.Map;
import java.util.HashMap;


public class Send{

	public static String post(Map data){
		String path="";

	
if(!data.containsKey("trankey")){
			HashMap res = new HashMap<String, String>();
			res.put("ErrorMessage","Missing trankey");
			return API.json_encode(res);
		}


		path = "/transactions/" + data.get("trankey") + "/send";

		try{
			return API.runCall("post",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
