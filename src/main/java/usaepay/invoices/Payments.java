package usaepay.invoices;

import usaepay.API;

import java.util.Map;
import java.util.HashMap;


public class Payments{

	public static String get(Map data){
		String path="";

	
if(!data.containsKey("invoice_key")){
			HashMap res = new HashMap<String, String>();
			res.put("ErrorMessage","Missing invoice_key");
			return API.json_encode(res);
		}

		HashMap params = new HashMap<String, String>();
		if(data.containsKey("limit")) params.put("limit",data.get("limit"));
		if(data.containsKey("offset")) params.put("offset",data.get("offset"));

		path = "/invoices/" + data.get("invoice_key") + "/payments";

		try{
			return API.runCall("get",path,data,params);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
