package usaepay.invoices;

import usaepay.API;

import java.util.Map;
import java.util.HashMap;


public class Cancel{

	public static String post(Map data){
		String path="";

	
if(!data.containsKey("invoice_key")){
			HashMap res = new HashMap<String, String>();
			res.put("ErrorMessage","Missing invoice_key");
			return API.json_encode(res);
		}


		path = "/invoices/" + data.get("invoice_key") + "/cancel";

		try{
			return API.runCall("post",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
