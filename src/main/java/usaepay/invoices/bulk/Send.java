package usaepay.invoices.bulk;

import usaepay.API;

import java.util.Map;
import java.util.HashMap;


public class Send{

	public static String post(Map data){
		String path="";


		path = "/invoices/bulk/send";

		try{
			return API.runCall("post",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
