package usaepay;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import usaepay.*;
import java.util.*;


public class transactionsgetTest {
	@Test public void transactionsget() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("type","list");
		expected_response.put("limit","0");
		expected_response.put("offset","20");
		List<Map> data = new ArrayList<Map>();
			Map<String, Object> Transaction = new HashMap<String, Object>();
			Transaction.put("type","transaction");
			Transaction.put("key","dnfwxwhz5kvnbgb");
			Transaction.put("refnum","100061");
			Transaction.put("created","2018-12-04 10:48:44");
			Transaction.put("trantype_code","S");
			Transaction.put("trantype","Credit Card Sale");
			Transaction.put("result_code","A");
			Transaction.put("result","Approved");
			Transaction.put("error_code","12");
			Transaction.put("error","Card Number was not between 13 and 16 digits");
			Transaction.put("authcode","314407");
			Transaction.put("status_code","P");
			Transaction.put("status","Authorized (Pending Settlement)");
			Transaction.put("proc_refnum","18120443975126");
				Map<String, Object> creditcard = new HashMap<String, Object>();
				creditcard.put("cardholder","Minerva McGonnegall");
				creditcard.put("number","4444xxxxxxxx1111");
				creditcard.put("category_code","A");
				creditcard.put("entry_mode","swiped");
				creditcard.put("avs_street","1234 Portkey Ave");
				creditcard.put("avs_postalcode","90006");
			Transaction.put("creditcard","creditcard");
				Map<String, Object> check = new HashMap<String, Object>();
				check.put("accountholder","Remus Lupin");
				check.put("routing","123456789");
				check.put("account","324523524");
				check.put("account_type","checking");
			Transaction.put("check","check");
				Map<String, Object> avs = new HashMap<String, Object>();
				avs.put("result_code","YYY");
				avs.put("result","Address: Match & 5 Digit Zip: Match");
			Transaction.put("avs","avs");
				Map<String, Object> cvc = new HashMap<String, Object>();
				cvc.put("result_code","N");
				cvc.put("result","No Match");
			Transaction.put("cvc","cvc");
				Map<String, Object> batch = new HashMap<String, Object>();
				batch.put("type","batch");
				batch.put("key","ft1m9m5p9wgd9mb");
				batch.put("batchrefnum","12345678");
				batch.put("sequence","2902");
			Transaction.put("batch","batch");
			Transaction.put("amount","50");
				Map<String, Object> amount_detail = new HashMap<String, Object>();
				amount_detail.put("subtotal","40.00");
				amount_detail.put("tax","10.00");
				amount_detail.put("enable_partialauth","false");
			Transaction.put("amount_detail","amount_detail");
			Transaction.put("ponum","af416fsd5");
			Transaction.put("invoice","98454685");
			Transaction.put("orderid","98454685");
			Transaction.put("description","Antique Pensieve");
				Map<String, Object> billing_address = new HashMap<String, Object>();
				billing_address.put("company","Hogwarts School of Witchcraft and Wizardry");
				billing_address.put("street","123 Astronomy Tower");
				billing_address.put("postalcode","10005");
			Transaction.put("billing_address","billing_address");
				Map<String, Object> shipping_address = new HashMap<String, Object>();
				shipping_address.put("company","Hogwarts School of Witchcraft and Wizardry");
				shipping_address.put("street","123 Astronomy Tower");
				shipping_address.put("postalcode","10005");
			Transaction.put("shipping_address","shipping_address");
			List<Map> lineitems = new ArrayList<Map>();
				Map<String, Object> LineItem = new HashMap<String, Object>();
				LineItem.put("name","Antique Pensieve");
				LineItem.put("description","See into your past...");
				LineItem.put("size","Large");
				LineItem.put("color","Bronze");
				LineItem.put("cost","450.00");
				LineItem.put("list_price","500.00");
				LineItem.put("qty","1");
				LineItem.put("sku","68453423");
				LineItem.put("commoditycode","535");
				LineItem.put("discountamount","0");
				LineItem.put("discountrate","0");
				LineItem.put("taxable","");
				LineItem.put("taxamount","45.00");
				LineItem.put("taxclass","NA");
				LineItem.put("taxrate","10");
				LineItem.put("um","EA");
				LineItem.put("category","Antiques");
				LineItem.put("manufacturer","The Ancients");
			lineitems.add(LineItem);
				Map<String, Object> LineItem1 = new HashMap<String, Object>();
				LineItem1.put("name","Antique Pensieve");
				LineItem1.put("description","See into your past...");
				LineItem1.put("size","Large");
				LineItem1.put("color","Bronze");
				LineItem1.put("cost","450.00");
				LineItem1.put("list_price","500.00");
				LineItem1.put("qty","1");
				LineItem1.put("sku","68453423");
				LineItem1.put("commoditycode","535");
				LineItem1.put("discountamount","0");
				LineItem1.put("discountrate","0");
				LineItem1.put("taxable","");
				LineItem1.put("taxamount","45.00");
				LineItem1.put("taxclass","NA");
				LineItem1.put("taxrate","10");
				LineItem1.put("um","EA");
				LineItem1.put("category","Antiques");
				LineItem1.put("manufacturer","The Ancients");
			lineitems.add(LineItem1);
			Transaction.put("lineitems",lineitems);
			List<Map> custom_fields = new ArrayList<Map>();
			Transaction.put("custom_fields",custom_fields);
			Transaction.put("comments","Powerful magical object. Use with caution.");
			Transaction.put("tranterm","multilane");
			Transaction.put("clerk","Madame Malkin");
				Map<String, Object> receipts = new HashMap<String, Object>();
				receipts.put("customer","Mail Sent Successfully");
				receipts.put("merchant","Mail Sent Successfully");
			Transaction.put("receipts","receipts");
				Map<String, Object> customer = new HashMap<String, Object>();
				customer.put("custkey","ksddgpqgpbs5zkmb");
				customer.put("custid","10275538");
			Transaction.put("customer","customer");
			Transaction.put("customer_email","noreply@gmail.com");
				Map<String, Object> bin = new HashMap<String, Object>();
				bin.put("bin","444455");
				bin.put("type","Debit");
				bin.put("issuer","Visa");
				bin.put("bank","Chase Bank");
				bin.put("country","US");
				bin.put("country_name","United States");
				bin.put("country_iso","CA");
				bin.put("location","http://www.jpmorganchase.com");
				bin.put("phone","USA");
				bin.put("category","");
			Transaction.put("bin","bin");
			Transaction.put("clientip","Madame Malkin");
			Transaction.put("source_name","Madame Malkin");
		data.add(Transaction);
			Map<String, Object> Transaction2 = new HashMap<String, Object>();
			Transaction2.put("type","transaction");
			Transaction2.put("key","dnfwxwhz5kvnbgb");
			Transaction2.put("refnum","100061");
			Transaction2.put("created","2018-12-04 10:48:44");
			Transaction2.put("trantype_code","S");
			Transaction2.put("trantype","Credit Card Sale");
			Transaction2.put("result_code","A");
			Transaction2.put("result","Approved");
			Transaction2.put("error_code","12");
			Transaction2.put("error","Card Number was not between 13 and 16 digits");
			Transaction2.put("authcode","314407");
			Transaction2.put("status_code","P");
			Transaction2.put("status","Authorized (Pending Settlement)");
			Transaction2.put("proc_refnum","18120443975126");
				Map<String, Object> creditcard3 = new HashMap<String, Object>();
				creditcard.put("cardholder","Minerva McGonnegall");
				creditcard.put("number","4444xxxxxxxx1111");
				creditcard.put("category_code","A");
				creditcard.put("entry_mode","swiped");
				creditcard.put("avs_street","1234 Portkey Ave");
				creditcard.put("avs_postalcode","90006");
			Transaction2.put("creditcard","creditcard");
				Map<String, Object> check4 = new HashMap<String, Object>();
				check.put("accountholder","Remus Lupin");
				check.put("routing","123456789");
				check.put("account","324523524");
				check.put("account_type","checking");
			Transaction2.put("check","check");
				Map<String, Object> avs5 = new HashMap<String, Object>();
				avs.put("result_code","YYY");
				avs.put("result","Address: Match & 5 Digit Zip: Match");
			Transaction2.put("avs","avs");
				Map<String, Object> cvc6 = new HashMap<String, Object>();
				cvc.put("result_code","N");
				cvc.put("result","No Match");
			Transaction2.put("cvc","cvc");
				Map<String, Object> batch7 = new HashMap<String, Object>();
				batch.put("type","batch");
				batch.put("key","ft1m9m5p9wgd9mb");
				batch.put("batchrefnum","12345678");
				batch.put("sequence","2902");
			Transaction2.put("batch","batch");
			Transaction2.put("amount","50");
				Map<String, Object> amount_detail8 = new HashMap<String, Object>();
				amount_detail.put("subtotal","40.00");
				amount_detail.put("tax","10.00");
				amount_detail.put("enable_partialauth","false");
			Transaction2.put("amount_detail","amount_detail");
			Transaction2.put("ponum","af416fsd5");
			Transaction2.put("invoice","98454685");
			Transaction2.put("orderid","98454685");
			Transaction2.put("description","Antique Pensieve");
				Map<String, Object> billing_address9 = new HashMap<String, Object>();
				billing_address.put("company","Hogwarts School of Witchcraft and Wizardry");
				billing_address.put("street","123 Astronomy Tower");
				billing_address.put("postalcode","10005");
			Transaction2.put("billing_address","billing_address");
				Map<String, Object> shipping_address10 = new HashMap<String, Object>();
				shipping_address.put("company","Hogwarts School of Witchcraft and Wizardry");
				shipping_address.put("street","123 Astronomy Tower");
				shipping_address.put("postalcode","10005");
			Transaction2.put("shipping_address","shipping_address");
			List<Map> lineitems11 = new ArrayList<Map>();
				Map<String, Object> LineItem12 = new HashMap<String, Object>();
				LineItem12.put("name","Antique Pensieve");
				LineItem12.put("description","See into your past...");
				LineItem12.put("size","Large");
				LineItem12.put("color","Bronze");
				LineItem12.put("cost","450.00");
				LineItem12.put("list_price","500.00");
				LineItem12.put("qty","1");
				LineItem12.put("sku","68453423");
				LineItem12.put("commoditycode","535");
				LineItem12.put("discountamount","0");
				LineItem12.put("discountrate","0");
				LineItem12.put("taxable","");
				LineItem12.put("taxamount","45.00");
				LineItem12.put("taxclass","NA");
				LineItem12.put("taxrate","10");
				LineItem12.put("um","EA");
				LineItem12.put("category","Antiques");
				LineItem12.put("manufacturer","The Ancients");
			lineitems11.add(LineItem12);
				Map<String, Object> LineItem13 = new HashMap<String, Object>();
				LineItem13.put("name","Antique Pensieve");
				LineItem13.put("description","See into your past...");
				LineItem13.put("size","Large");
				LineItem13.put("color","Bronze");
				LineItem13.put("cost","450.00");
				LineItem13.put("list_price","500.00");
				LineItem13.put("qty","1");
				LineItem13.put("sku","68453423");
				LineItem13.put("commoditycode","535");
				LineItem13.put("discountamount","0");
				LineItem13.put("discountrate","0");
				LineItem13.put("taxable","");
				LineItem13.put("taxamount","45.00");
				LineItem13.put("taxclass","NA");
				LineItem13.put("taxrate","10");
				LineItem13.put("um","EA");
				LineItem13.put("category","Antiques");
				LineItem13.put("manufacturer","The Ancients");
			lineitems11.add(LineItem13);
			Transaction2.put("lineitems",lineitems11);
			List<Map> custom_fields14 = new ArrayList<Map>();
			Transaction2.put("custom_fields",custom_fields14);
			Transaction2.put("comments","Powerful magical object. Use with caution.");
			Transaction2.put("tranterm","multilane");
			Transaction2.put("clerk","Madame Malkin");
				Map<String, Object> receipts15 = new HashMap<String, Object>();
				receipts.put("customer","Mail Sent Successfully");
				receipts.put("merchant","Mail Sent Successfully");
			Transaction2.put("receipts","receipts");
				Map<String, Object> customer16 = new HashMap<String, Object>();
				customer.put("custkey","ksddgpqgpbs5zkmb");
				customer.put("custid","10275538");
			Transaction2.put("customer","customer");
			Transaction2.put("customer_email","noreply@gmail.com");
				Map<String, Object> bin17 = new HashMap<String, Object>();
				bin.put("bin","444455");
				bin.put("type","Debit");
				bin.put("issuer","Visa");
				bin.put("bank","Chase Bank");
				bin.put("country","US");
				bin.put("country_name","United States");
				bin.put("country_iso","CA");
				bin.put("location","http://www.jpmorganchase.com");
				bin.put("phone","USA");
				bin.put("category","");
			Transaction2.put("bin","bin");
			Transaction2.put("clientip","Madame Malkin");
			Transaction2.put("source_name","Madame Malkin");
		data.add(Transaction2);
		expected_response.put("data",data);
		expected_response.put("total","42");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("limit","Examplelimit");
		reqdata.put("offset","Exampleoffset");
		reqdata.put("fuzzy","Examplefuzzy");
		reqdata.put("filters","exampleFilter");
		String response = "";
		response = usaepay.Transactions.get(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://secure.usaepay.com/api/v2/transactions"));
		assertTrue("Path missing query parameter limit",MockHandler.path.contains("Examplelimit"));
		assertTrue("Path missing query parameter offset",MockHandler.path.contains("Exampleoffset"));
		assertTrue("Path missing query parameter fuzzy",MockHandler.path.contains("Examplefuzzy"));
		assertTrue("Path missing query parameter filters",MockHandler.path.contains("exampleFilter"));
		assertEquals("Method did not match ","GET",MockHandler.method.toUpperCase());
	}
}