package usaepay;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import usaepay.*;
import java.util.*;


public class productscategoriespostTest {
	@Test public void productscategoriespost() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("type","product_category");
		expected_response.put("key","80v96p6rx2k6sktw");
		expected_response.put("name","Botts Every Flavor Beans");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
			reqdata.put("name","Botts Every Flavor Beans");
		String response = "";
		response = usaepay.products.Categories.post(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://secure.usaepay.com/api/v2/products/categories"));
		assertEquals("Method did not match ","POST",MockHandler.method.toUpperCase());
	}
}