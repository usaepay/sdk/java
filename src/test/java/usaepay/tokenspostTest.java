package usaepay;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import usaepay.*;
import java.util.*;


public class tokenspostTest {
	@Test public void tokenspost() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("type","token");
		expected_response.put("key","p4xe-9ax3-26f4-pi6h");
			Map<String, Object> token = new HashMap<String, Object>();
			Map<String, Object> token1 = new HashMap<String, Object>();
			token1.put("type","token");
			token1.put("key","p4xe-9ax3-26f4-pi6h");
			token1.put("card_key","p4xe-9ax3-26f4-pi6h");
			token1.put("cardref","p4xe-9ax3-26f4-pi6h");
			token1.put("masked_card_number","XXXXXXXXXXXX2226");
			token1.put("card_type","Visa");
		expected_response.put("token","token");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
				Map<String, Object> creditcard = new HashMap<String, Object>();
				creditcard.put("cardholder","Minerva McGonnegall");
				creditcard.put("number","4000100011112224");
				creditcard.put("expiration","1222");
			reqdata.put("creditcard","creditcard");
				Map<String, Object> customer = new HashMap<String, Object>();
				customer.put("custkey","9sddg0ccd915pdbq");
				customer.put("paymethod_key","2n02pc6k26jkfkh1b");
			reqdata.put("customer","customer");
			List<Map> list = new ArrayList<Map>();
				Map<String, Object> CustomerTokenRequest = new HashMap<String, Object>();
				CustomerTokenRequest.put("custkey","9sddg0ccd915pdbq");
				CustomerTokenRequest.put("paymethod_key","2n02pc6k26jkfkh1b");
			list.add(CustomerTokenRequest);
				Map<String, Object> CustomerTokenRequest2 = new HashMap<String, Object>();
				CustomerTokenRequest2.put("custkey","9sddg0ccd915pdbq");
				CustomerTokenRequest2.put("paymethod_key","2n02pc6k26jkfkh1b");
			list.add(CustomerTokenRequest2);
			reqdata.put("list",list);
		String response = "";
		response = usaepay.Tokens.post(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://secure.usaepay.com/api/v2/tokens"));
		assertEquals("Method did not match ","POST",MockHandler.method.toUpperCase());
	}
}