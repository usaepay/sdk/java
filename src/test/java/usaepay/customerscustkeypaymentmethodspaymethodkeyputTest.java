package usaepay;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import usaepay.*;
import java.util.*;


public class customerscustkeypaymentmethodspaymethodkeyputTest {
	@Test public void customerscustkeypaymentmethodspaymethodkeyput() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("key","x8KccrxeydHJ4MmT");
		expected_response.put("type","customerpaymentmethod");
		expected_response.put("method_name","Example method");
		expected_response.put("cardholder","Testor Jones");
		expected_response.put("expiration","1221");
		expected_response.put("ccnum4last","xxxxxxxxxxxxxx7779");
		expected_response.put("card_type","Visa");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("custkey","Examplecustkey");
		reqdata.put("paymethod_key","Examplepaymethod_key");
			reqdata.put("cardholder","Fillius Flitwick");
			reqdata.put("expiration","1222");
			reqdata.put("number","4000100011112224");
			reqdata.put("pay_type","cc");
		String response = "";
		response = usaepay.customers.Payment_methods.put(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://secure.usaepay.com/api/v2/customers/Examplecustkey/payment_methods/Examplepaymethod_key"));
		assertEquals("Method did not match ","PUT",MockHandler.method.toUpperCase());
	}
}