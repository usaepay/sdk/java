package usaepay;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import usaepay.*;
import java.util.*;


public class paymentenginedevicesdevicekeykickpostTest {
	@Test public void paymentenginedevicesdevicekeykickpost() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("type","device");
		expected_response.put("key","sa_WKwzyQawBG0RMy0XpDGFXb6pXA32r");
		expected_response.put("apikeyid","ntC8nP31Moh0wtvYT");
		expected_response.put("terminal_type","standalone");
		expected_response.put("status","ntC8nP31Moh0wtvYT");
		expected_response.put("name","ntC8nP31Moh0wtvYT");
			Map<String, Object> settings = new HashMap<String, Object>();
			Map<String, Object> settings1 = new HashMap<String, Object>();
			settings1.put("timeout","30");
			settings1.put("enable_standalone","false");
			settings1.put("share_device","false");
			settings1.put("notify_update","true");
			settings1.put("notify_update_next","true");
			settings1.put("sleep_battery_device","5");
			settings1.put("sleep_battery_display","3");
			settings1.put("sleep_powered_device","10");
			settings1.put("sleep_powered_display","8");
		expected_response.put("settings","settings");
			Map<String, Object> terminal_info = new HashMap<String, Object>();
			Map<String, Object> terminal_info2 = new HashMap<String, Object>();
			terminal_info2.put("make","Castle");
			terminal_info2.put("model","Vega3000");
			terminal_info2.put("revision","18043001-0055.00");
			terminal_info2.put("serial","011118170300198");
			terminal_info2.put("key_pin","FFFF5B04");
			terminal_info2.put("key_pan","FF908A");
		expected_response.put("terminal_info","terminal_info");
			Map<String, Object> terminal_config = new HashMap<String, Object>();
			Map<String, Object> terminal_config3 = new HashMap<String, Object>();
			terminal_config3.put("enable_emv","true");
			terminal_config3.put("enable_debit_msr","true");
			terminal_config3.put("enable_tip_adjust","true");
			terminal_config3.put("enable_contactless","true");
		expected_response.put("terminal_config","terminal_config");
		expected_response.put("pairing_code","690520");
		expected_response.put("expiration","2019-10-29 11:59:28");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("devicekey","Exampledevicekey");
		String response = "";
		response = usaepay.paymentengine.devices.Kick.post(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://secure.usaepay.com/api/v2/paymentengine/devices/Exampledevicekey/kick"));
		assertEquals("Method did not match ","POST",MockHandler.method.toUpperCase());
	}
}