package usaepay;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import usaepay.*;
import java.util.*;


public class paymentenginedevicesdevicekeysettingsgetTest {
	@Test public void paymentenginedevicesdevicekeysettingsget() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("timeout","30");
		expected_response.put("enable_standalone","false");
		expected_response.put("share_device","false");
		expected_response.put("notify_update","true");
		expected_response.put("notify_update_next","true");
		expected_response.put("sleep_battery_device","5");
		expected_response.put("sleep_battery_display","3");
		expected_response.put("sleep_powered_device","10");
		expected_response.put("sleep_powered_display","8");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("devicekey","Exampledevicekey");
		String response = "";
		response = usaepay.paymentengine.devices.Settings.get(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://secure.usaepay.com/api/v2/paymentengine/devices/Exampledevicekey/settings"));
		assertEquals("Method did not match ","GET",MockHandler.method.toUpperCase());
	}
}