package usaepay;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import usaepay.*;
import java.util.*;


public class customersgetTest {
	@Test public void customersget() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("type","list");
		expected_response.put("limit","0");
		expected_response.put("offset","20");
		List<Map> data = new ArrayList<Map>();
			Map<String, Object> CustomerSummary = new HashMap<String, Object>();
			CustomerSummary.put("type","customer");
			CustomerSummary.put("key","nsds9yd1h5kb9y5h");
			CustomerSummary.put("custid","123456");
			CustomerSummary.put("company","Weasleys Wizard Wheezes");
			CustomerSummary.put("first_name","George");
			CustomerSummary.put("last_name","Weasley");
			CustomerSummary.put("address","93 Diagon Alley");
			CustomerSummary.put("address2","Apartment 2");
			CustomerSummary.put("city","London");
			CustomerSummary.put("state","Greater London");
			CustomerSummary.put("postalcode","WC2H 0AW");
			CustomerSummary.put("country","Great Britain");
			CustomerSummary.put("phone","747-3733");
			CustomerSummary.put("fax","584-4537");
			CustomerSummary.put("email","noreply@mugglenet.com");
			CustomerSummary.put("url","https://www.pottermore.com/");
			CustomerSummary.put("notes","Born on April fools day.");
			CustomerSummary.put("description","Mischief Managed.");
		data.add(CustomerSummary);
			Map<String, Object> CustomerSummary1 = new HashMap<String, Object>();
			CustomerSummary1.put("type","customer");
			CustomerSummary1.put("key","nsds9yd1h5kb9y5h");
			CustomerSummary1.put("custid","123456");
			CustomerSummary1.put("company","Weasleys Wizard Wheezes");
			CustomerSummary1.put("first_name","George");
			CustomerSummary1.put("last_name","Weasley");
			CustomerSummary1.put("address","93 Diagon Alley");
			CustomerSummary1.put("address2","Apartment 2");
			CustomerSummary1.put("city","London");
			CustomerSummary1.put("state","Greater London");
			CustomerSummary1.put("postalcode","WC2H 0AW");
			CustomerSummary1.put("country","Great Britain");
			CustomerSummary1.put("phone","747-3733");
			CustomerSummary1.put("fax","584-4537");
			CustomerSummary1.put("email","noreply@mugglenet.com");
			CustomerSummary1.put("url","https://www.pottermore.com/");
			CustomerSummary1.put("notes","Born on April fools day.");
			CustomerSummary1.put("description","Mischief Managed.");
		data.add(CustomerSummary1);
		expected_response.put("data",data);
		expected_response.put("total","1");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("limit","Examplelimit");
		reqdata.put("offset","Exampleoffset");
		String response = "";
		response = usaepay.Customers.get(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://secure.usaepay.com/api/v2/customers"));
		assertTrue("Path missing query parameter limit",MockHandler.path.contains("Examplelimit"));
		assertTrue("Path missing query parameter offset",MockHandler.path.contains("Exampleoffset"));
		assertEquals("Method did not match ","GET",MockHandler.method.toUpperCase());
	}
}