package usaepay;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import usaepay.*;
import java.util.*;


public class batchescurrentclosepostTest {
	@Test public void batchescurrentclosepost() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("type","batch");
		expected_response.put("key","ft1m9m5p9wgd9mb");
		expected_response.put("batchnum","2412");
		expected_response.put("opened","2018-11-24 11:02:02");
		expected_response.put("closed","2018-11-24 12:01:01");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		String response = "";
		response = usaepay.batches.current.Close.post(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://secure.usaepay.com/api/v2/batches/current/close"));
		assertEquals("Method did not match ","POST",MockHandler.method.toUpperCase());
	}
}