package usaepay;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import usaepay.*;
import java.util.*;


public class tokenscardrefgetTest {
	@Test public void tokenscardrefget() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("type","token");
		expected_response.put("key","p4xe-9ax3-26f4-pi6h");
		expected_response.put("card_key","p4xe-9ax3-26f4-pi6h");
		expected_response.put("cardref","p4xe-9ax3-26f4-pi6h");
		expected_response.put("masked_card_number","XXXXXXXXXXXX2226");
		expected_response.put("card_type","Visa");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("cardref","Examplecardref");
		String response = "";
		response = usaepay.Tokens.get(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://secure.usaepay.com/api/v2/tokens/Examplecardref"));
		assertEquals("Method did not match ","GET",MockHandler.method.toUpperCase());
	}
}