package usaepay;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import usaepay.*;
import java.util.*;


public class batchesgetTest {
	@Test public void batchesget() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("type","list");
		expected_response.put("limit","0");
		expected_response.put("offset","20");
		List<Map> data = new ArrayList<Map>();
			Map<String, Object> BatchSummary = new HashMap<String, Object>();
			BatchSummary.put("type","batch");
			BatchSummary.put("key","ft1m9m5p9wgd9mb");
			BatchSummary.put("batchnum","2412");
			BatchSummary.put("opened","2018-11-24 11:02:02");
			BatchSummary.put("closed","2018-11-24 12:01:01");
		data.add(BatchSummary);
			Map<String, Object> BatchSummary1 = new HashMap<String, Object>();
			BatchSummary1.put("type","batch");
			BatchSummary1.put("key","ft1m9m5p9wgd9mb");
			BatchSummary1.put("batchnum","2412");
			BatchSummary1.put("opened","2018-11-24 11:02:02");
			BatchSummary1.put("closed","2018-11-24 12:01:01");
		data.add(BatchSummary1);
		expected_response.put("data",data);
		expected_response.put("total","1");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("limit","Examplelimit");
		reqdata.put("offset","Exampleoffset");
		reqdata.put("openedlt","Exampleopenedlt");
		reqdata.put("openedgt","Exampleopenedgt");
		reqdata.put("closedlt","Exampleclosedlt");
		reqdata.put("closedgt","Exampleclosedgt");
		reqdata.put("openedle","Exampleopenedle");
		reqdata.put("openedge","Exampleopenedge");
		reqdata.put("closedle","Exampleclosedle");
		reqdata.put("closedge","Exampleclosedge");
		String response = "";
		response = usaepay.Batches.get(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://secure.usaepay.com/api/v2/batches"));
		assertTrue("Path missing query parameter limit",MockHandler.path.contains("Examplelimit"));
		assertTrue("Path missing query parameter offset",MockHandler.path.contains("Exampleoffset"));
		assertTrue("Path missing query parameter openedlt",MockHandler.path.contains("Exampleopenedlt"));
		assertTrue("Path missing query parameter openedgt",MockHandler.path.contains("Exampleopenedgt"));
		assertTrue("Path missing query parameter closedlt",MockHandler.path.contains("Exampleclosedlt"));
		assertTrue("Path missing query parameter closedgt",MockHandler.path.contains("Exampleclosedgt"));
		assertTrue("Path missing query parameter openedle",MockHandler.path.contains("Exampleopenedle"));
		assertTrue("Path missing query parameter openedge",MockHandler.path.contains("Exampleopenedge"));
		assertTrue("Path missing query parameter closedle",MockHandler.path.contains("Exampleclosedle"));
		assertTrue("Path missing query parameter closedge",MockHandler.path.contains("Exampleclosedge"));
		assertEquals("Method did not match ","GET",MockHandler.method.toUpperCase());
	}
}