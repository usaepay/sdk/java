package usaepay;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import usaepay.*;
import java.util.*;


public class customerscustkeypaymentmethodspostTest {
	@Test public void customerscustkeypaymentmethodspost() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		List<Map> expected_response = new ArrayList<Map>();
		Map<String, Object> CustomerPaymentMethod = new HashMap<String, Object>();
			CustomerPaymentMethod.put("key","x8KccrxeydHJ4MmT");
			CustomerPaymentMethod.put("type","customerpaymentmethod");
			CustomerPaymentMethod.put("method_name","Example method");
			CustomerPaymentMethod.put("cardholder","Testor Jones");
			CustomerPaymentMethod.put("expiration","1221");
			CustomerPaymentMethod.put("ccnum4last","xxxxxxxxxxxxxx7779");
			CustomerPaymentMethod.put("card_type","Visa");
		expected_response.add(CustomerPaymentMethod);
		Map<String, Object> CustomerPaymentMethod1 = new HashMap<String, Object>();
			CustomerPaymentMethod1.put("key","x8KccrxeydHJ4MmT");
			CustomerPaymentMethod1.put("type","customerpaymentmethod");
			CustomerPaymentMethod1.put("method_name","Example method");
			CustomerPaymentMethod1.put("cardholder","Testor Jones");
			CustomerPaymentMethod1.put("expiration","1221");
			CustomerPaymentMethod1.put("ccnum4last","xxxxxxxxxxxxxx7779");
			CustomerPaymentMethod1.put("card_type","Visa");
		expected_response.add(CustomerPaymentMethod1);
		String encoded_response = API.json_encode_list(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("custkey","Examplecustkey");
			Map<String, Object> request_item0 = new HashMap<String, Object>();
			request_item0.put("cardholder","Fillius Flitwick");
			request_item0.put("expiration","1222");
			request_item0.put("number","4000100011112224");
			request_item0.put("pay_type","cc");
		reqdata.put("0",request_item0);
			Map<String, Object> request_item1 = new HashMap<String, Object>();
			request_item1.put("cardholder","Fillius Flitwick");
			request_item1.put("expiration","1222");
			request_item1.put("number","4000100011112224");
			request_item1.put("pay_type","cc");
		reqdata.put("1",request_item1);
		String response = "";
		response = usaepay.customers.Payment_methods.post(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://secure.usaepay.com/api/v2/customers/Examplecustkey/payment_methods"));
		assertEquals("Method did not match ","POST",MockHandler.method.toUpperCase());
	}
}