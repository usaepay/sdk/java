package usaepay;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import usaepay.*;
import java.util.*;


public class batchescurrentgetTest {
	@Test public void batchescurrentget() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("type","batch");
		expected_response.put("key","ft1m9m5p9wgd9mb");
		expected_response.put("opened","2019-01-28 17:02:22");
		expected_response.put("status","closed");
		expected_response.put("closed","2019-01-28 17:02:36");
		expected_response.put("scheduled","2019-01-28 17:02:36");
		expected_response.put("total_amount","3042.48");
		expected_response.put("total_count","29");
		expected_response.put("sales_amount","4755.48");
		expected_response.put("sales_count","18");
		expected_response.put("voids_amount","17.99");
		expected_response.put("voids_count","1");
		expected_response.put("refunds_amount","1713");
		expected_response.put("refunds_count","10");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		String response = "";
		response = usaepay.batches.Current.get(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://secure.usaepay.com/api/v2/batches/current"));
		assertEquals("Method did not match ","GET",MockHandler.method.toUpperCase());
	}
}