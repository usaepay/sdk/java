package usaepay;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import usaepay.*;
import java.util.*;


public class paymentenginepayrequestsrequestkeygetTest {
	@Test public void paymentenginepayrequestsrequestkeyget() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("type","request");
		expected_response.put("key","pr_3mW7rstrdA0Sp32LW9MN3djCITAhx");
		expected_response.put("expiration","2020-07-08 16:40:48");
		expected_response.put("status","sent to device");
			Map<String, Object> transaction = new HashMap<String, Object>();
			Map<String, Object> transaction1 = new HashMap<String, Object>();
			transaction1.put("type","transaction");
			transaction1.put("key","dnfwxwhz5kvnbgb");
			transaction1.put("refnum","100061");
			transaction1.put("created","2018-12-04 10:48:44");
			transaction1.put("is_duplicate","N");
			transaction1.put("result_code","A");
			transaction1.put("result","Approved");
			transaction1.put("authcode","314407");
			transaction1.put("proc_refnum","18120443975126");
				Map<String, Object> creditcard = new HashMap<String, Object>();
				Map<String, Object> creditcard2 = new HashMap<String, Object>();
				creditcard2.put("cardholder","Minerva McGonnegall");
				creditcard2.put("number","4444xxxxxxxx1111");
				creditcard2.put("category_code","A");
				creditcard2.put("entry_mode","swiped");
				creditcard2.put("avs_street","1234 Portkey Ave");
				creditcard2.put("avs_postalcode","90006");
			transaction.put("creditcard","creditcard");
			transaction1.put("invoice","98454685");
			transaction1.put("orderid","4556432");
				Map<String, Object> avs = new HashMap<String, Object>();
				Map<String, Object> avs3 = new HashMap<String, Object>();
				avs3.put("result_code","YYY");
				avs3.put("result","Address: Match & 5 Digit Zip: Match");
			transaction.put("avs","avs");
				Map<String, Object> cvc = new HashMap<String, Object>();
				Map<String, Object> cvc4 = new HashMap<String, Object>();
				cvc4.put("result_code","N");
				cvc4.put("result","No Match");
			transaction.put("cvc","cvc");
				Map<String, Object> batch = new HashMap<String, Object>();
				Map<String, Object> batch5 = new HashMap<String, Object>();
				batch5.put("type","batch");
				batch5.put("key","ft1m9m5p9wgd9mb");
				batch5.put("batchrefnum","12345678");
				batch5.put("sequence","2902");
			transaction.put("batch","batch");
			transaction1.put("auth_amount","500.00");
			transaction1.put("trantype","Credit Card Sale");
			transaction1.put("iccdata","");
				Map<String, Object> receipts = new HashMap<String, Object>();
				Map<String, Object> receipts6 = new HashMap<String, Object>();
				receipts6.put("customer","Mail Sent Successfully");
				receipts6.put("merchant","Mail Sent Successfully");
			transaction.put("receipts","receipts");
				Map<String, Object> customer = new HashMap<String, Object>();
				Map<String, Object> customer7 = new HashMap<String, Object>();
				customer7.put("custkey","ksddgpqgpbs5zkmb");
				customer7.put("custid","10275538");
			transaction.put("customer","customer");
				Map<String, Object> savedcard = new HashMap<String, Object>();
				Map<String, Object> savedcard8 = new HashMap<String, Object>();
				savedcard8.put("type","Visa");
				savedcard8.put("key","tyet-xk8x-vpj4-7swq");
				savedcard8.put("cardnumber","4000xxxxxxxx2224");
			transaction.put("savedcard","savedcard");
		expected_response.put("transaction","transaction");
		expected_response.put("complete","sent to device");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("requestkey","Examplerequestkey");
		String response = "";
		response = usaepay.paymentengine.Payrequests.get(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://secure.usaepay.com/api/v2/paymentengine/payrequests/Examplerequestkey"));
		assertEquals("Method did not match ","GET",MockHandler.method.toUpperCase());
	}
}