package usaepay;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import usaepay.*;
import java.util.*;


public class productscategoriesgetTest {
	@Test public void productscategoriesget() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("type","list");
		expected_response.put("limit","0");
		expected_response.put("offset","20");
		List<Map> data = new ArrayList<Map>();
			Map<String, Object> ProductCategoryResponse = new HashMap<String, Object>();
			ProductCategoryResponse.put("type","product_category");
			ProductCategoryResponse.put("key","80v96p6rx2k6sktw");
			ProductCategoryResponse.put("name","Botts Every Flavor Beans");
		data.add(ProductCategoryResponse);
			Map<String, Object> ProductCategoryResponse1 = new HashMap<String, Object>();
			ProductCategoryResponse1.put("type","product_category");
			ProductCategoryResponse1.put("key","80v96p6rx2k6sktw");
			ProductCategoryResponse1.put("name","Botts Every Flavor Beans");
		data.add(ProductCategoryResponse1);
		expected_response.put("data",data);
		expected_response.put("total","42");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("limit","Examplelimit");
		reqdata.put("offset","Exampleoffset");
		String response = "";
		response = usaepay.products.Categories.get(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://secure.usaepay.com/api/v2/products/categories"));
		assertTrue("Path missing query parameter limit",MockHandler.path.contains("Examplelimit"));
		assertTrue("Path missing query parameter offset",MockHandler.path.contains("Exampleoffset"));
		assertEquals("Method did not match ","GET",MockHandler.method.toUpperCase());
	}
}